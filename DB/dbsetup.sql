CREATE DATABASE titanic;

USE titanic;

CREATE TABLE People (
	Survived boolean NOT NULL,
	PassengerClass numeric NOT NULL,
	Name Text NOT NULL,
	Sex Text NOT NULL,
	Age decimal NOT NULL,
	SiblingsOrSpousesAboard int NOT NULL,
	ParentsOrChildrenAboard int NOT NULL,
	Fare decimal NOT NULL
);
#Load titanic.csv file to People table
LOAD DATA INFILE '/var/lib/mysql-files/titanic.csv'
INTO TABLE People
FIELDS TERMINATED BY ','
IGNORE 1 ROWS;

SET SQL_SAFE_UPDATES = 0;

ALTER TABLE People 
ADD COLUMN Uuid VARCHAR(45);

UPDATE People SET Uuid=(SELECT uuid());